#!/usr/bin/env python
# Copyright (C) 2009 Sebastian Pipping <sebastian@pipping.org>
# Licensed under GPL 2 or later

import sys
import os
if len(sys.argv) != 1 + 2:
    print "USAGE:\n  python  %s  foo/repositories.xml bar/layman-global.txt" % \
            os.path.basename(sys.argv[0])
    sys.exit(1)
repositories_xml_location = sys.argv[1]
layman_global_txt_location = sys.argv[2]


import xml.etree.ElementTree as ET
from layman.dbtools.sharedutils import * # local

a = ET.parse(open(repositories_xml_location))
repositories = a.getroot()

for repo in repositories:
    # Transform 'repo' tag
    repo.tag = 'overlay'

    # Transform 'homepage' tag
    homepage = repo.find('homepage')
    if homepage != None:
        homepage.tag = 'link'
        # Move 'link' tag first
        repo.remove(homepage)
        repo.insert(0, homepage)

    # Transform 'owner' tag
    owner = repo.find('owner')
    repo.attrib['contact'] = owner.find('email').text
    repo.remove(owner)

    # Kill 'feed' tags
    for o in repo.findall('feed'):
        repo.remove(o)

    first_source = repo.find('source')
    repo.attrib['src'] = first_source.text
    repo.attrib['type'] = first_source.attrib['type']
    repo.remove(first_source)

    # Kill unsupported attributes
    for att_name in ('quality', ):
        if att_name in repo.attrib:
            del repo.attrib[att_name]

    # Transform 'description' tags
    descriptions_kept = 0
    for description in repo.findall('description'):
        if 'lang' in description.attrib.keys():
            if description.attrib['lang'] not in ('C', 'en') or \
                    descriptions_kept > 0:
                repo.remove(description)
            else:
                del description.attrib['lang']
                descriptions_kept = descriptions_kept + 1

    # Transform 'name' tag
    name = repo.find('name')
    repo.attrib['name'] = name.text
    repo.remove(name)

    # Kill unsupported tags
    for tag_name in ('feed', 'source',  'longdescription'):
        for o in repo.findall(tag_name):
            repo.remove(o)

    # Add note on file being a generated one
    repo.insert(0, ET.Comment('THIS FILE IS GENERATED, PLEASE EDIT repositories.xml INSTEAD.'))

# Kill unsupported attributes
for att_name in ('version', ):
    if att_name in repositories.attrib:
        del repositories.attrib[att_name]

# Transform 'repositories' tag
repositories.tag = 'layman'



# recurse_print(repositories)
indent(repositories)
layman_global_txt = open(layman_global_txt_location, 'w')
layman_global_txt.write("""\
<?xml version="1.0" encoding="UTF-8"?>
<!-- $Header$ -->
""")
a.write(layman_global_txt, encoding='utf-8')
layman_global_txt.close()
