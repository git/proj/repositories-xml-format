#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2009 Sebastian Pipping <sebastian@pipping.org>
# Licensed under GPL 2 or later

import sys
import os
import datetime
if len(sys.argv) != 1 + 3:
    print "USAGE:\n  python  %s  foo/repositories.xml bar/overlays.base baz/overlays.ini" % \
            os.path.basename(sys.argv[0])
    sys.exit(1)
repositories_xml_location = sys.argv[1]
overlays_base_location = sys.argv[2]
overlays_ini_location = sys.argv[3]


import xml.etree.ElementTree as ET
from ConfigParser import ConfigParser, DuplicateSectionError
from layman.dbtools.sharedutils import * # local

a = ET.parse(open(repositories_xml_location))
repositories = a.getroot()

overlays_ini = ConfigParser()

feed_uri_to_name = {}

for repo in repositories:
    try:
        _feed_uri = repo.find('feed').text.strip()
    except AttributeError:
        continue

    repo_name = repo.find('name').text.strip()
    if _feed_uri in feed_uri_to_name:
        feed_uri_to_name[_feed_uri].add(repo_name)
    else:
        feed_uri_to_name[_feed_uri] = set([repo_name])

    if repo.attrib.get('status', 'unofficial') != 'official':
        print '    Info: Skipping unofficial overlay "%s"' % repo_name
        continue

    def shorten_down(l):
        pos = l[0].find('-')
        if pos != -1:
            # e.g. on ['vdr-devel', 'vdr-experimental']
            prefix = l[0][0:pos]
        else:
            # e.g. on ['wschlich', 'wschlich-testing']
            prefix = l[0]
        if all(map(lambda x: x.startswith(prefix), l)):
            return '%s*' % prefix
        else:
            if all(map(lambda x: x.endswith('emacs'), l)):
                return 'emacs'
            return '/'.join(l)

    try:
        overlays_ini.add_section(_feed_uri)
    except DuplicateSectionError:
        # print 'Warning: Feed URI collision on <%s>' % _feed_uri
        _names = sorted(feed_uri_to_name[_feed_uri])
        repo_name = shorten_down(_names)
        print '    Info: Making name "%s" from "%s"' % (repo_name, '/'.join(_names))
        del _names

    overlays_ini.set(_feed_uri, 'name', repo_name)

    _owner_type = repo.find('owner').attrib.get('type', 'project')
    if _owner_type == 'person':
        overlays_ini.set(_feed_uri, 'developer', 'yes')
    else: # TODO elif _owner_type == 'project':
        overlays_ini.set(_feed_uri, 'project', 'yes')

    try:
        overlays_ini.set(_feed_uri, 'link', repo.find('homepage').text.strip())
    except AttributeError:
        print ' Warning: %s is missing a homepage' % repo_name

f = open(overlays_ini_location, 'w')
f.write("""\
# NOTE: This file has been GENERATED, DO NOT EDIT DIRECTLY.
#
# Date:
#   %(date)s
#
# Sources:
#   - %(overlays_ini_base)s
#   - %(repositories_xml)s
#
# The code of the generator script (%(script)s) is up here:
# http://git.overlays.gentoo.org/gitweb/?p=proj/repositories-xml-format.git;a=summary

""" % {
	'date':str(datetime.date.today()),
	'script':os.path.basename(sys.argv[0]),
	'overlays_ini_base':os.path.basename(overlays_base_location),
	'repositories_xml':os.path.basename(repositories_xml_location)})
g = open(overlays_base_location, 'r')
f.write(g.read())
g.close()
overlays_ini.write(f)
f.close()
