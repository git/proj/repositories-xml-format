#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2009 Sebastian Pipping <sebastian@pipping.org>
# Licensed under GPL 2 or later

import sys
import os
from optparse import OptionParser


VERSION = '0.9'
USAGE = """
  %prog  [OPTIONS]  [foo/layman-global.txt [bar/repositories.xml]]"""


parser = OptionParser(usage=USAGE, version=VERSION)
parser.add_option('-H', '--human',
    dest = 'human',
    default = False,
    action = 'store_true',
    help = 'more human friendly output instead of XML')
(opts, args) = parser.parse_args()

if len(args) > 1:
    layman_global_txt_location = args[1]
else:
    layman_global_txt_location = '/dev/stdin'

if len(args) > 2:
    repositories_xml_location = args[2]
else:
    repositories_xml_location = '/dev/stdout'


import xml.etree.ElementTree as ET
import codecs

from layman.dbtools.extradata import * # local
from layman.dbtools.feedextractors import * # local
from layman.dbtools.sharedutils import * # local

def to_ascii(o, current_encoding='utf-8'):
    if not isinstance(o, basestring):
        return o

    if isinstance(o, unicode):
        s = o
    else:
        s = unicode(o, current_encoding)
    return codecs.encode(s, 'ascii', 'ignore')

def append_feed(feed_uri, overlay_object):
    feed = ET.Element('feed')
    feed.text = feed_uri
    overlay_object.append(feed)

def strip(node):
    node.text = node.text.strip()

a = ET.parse(open(layman_global_txt_location))
overlays = a.getroot()

for overlay in overlays:
    repo_name = overlay.attrib['name']
    extra_data = TRANSITION_DATA_EXTRA.get(repo_name, {})

    # Move (and strip) 'description' tag
    description = overlay.find('description')
    overlay.remove(description)
    overlay.insert(0, description)
    strip(description)

    # Transform 'overlay' tag
    overlay.tag = 'repo'
    del overlay.attrib['name']
    name = ET.Element('name')
    name.text = repo_name
    overlay.insert(0, name)

    # Transform 'link' tag
    link = overlay.find('link')
    if link != None:
        link.tag = 'homepage'
        strip(link)

    # Transform 'contact' attribute
    owner = ET.Element('owner')
    overlay.append(owner)
    email = ET.Element('email')
    email.text = extra_data.get('owner', {}).\
            get('email', overlay.attrib['contact'])
    del overlay.attrib['contact']
    owner.append(email)

    # Transform 'src' and 'type' attribute
    source = ET.Element('source')
    source.text = overlay.attrib['src']
    del overlay.attrib['src']
    source.attrib['type'] = overlay.attrib['type']
    del overlay.attrib['type']
    overlay.append(source)

    # Extend by quality label
    try:
        overlay.attrib['quality'] = extra_data['quality']
    except KeyError:
        pass

    # Extend by owner type
    if repo_name in TRANSITION_DATA_PROJECTS:
        owner.attrib['type'] = 'project'

    # Extend by owner name
    try:
        maint_name = extra_data['owner']['name']
        name = ET.Element('name')
        name.text = maint_name
        owner.append(name)
    except KeyError:
        pass

    # Extend by feed URIs
    for fe in FEED_EXTRACTORS:
        uri = fe['regex'].sub(fe['format'], source.text)
        if uri != source.text:
            append_feed(uri, overlay)
            break
    try:
        feed_uris = extra_data['feeds']
    except KeyError:
        feed_uris = ()
    for uri in feed_uris:
        append_feed(uri, overlay)

    # Explicify defaults
    if 'status' not in overlay.attrib:
        overlay.attrib['status'] = 'unofficial'
    if 'quality' not in overlay.attrib:
        overlay.attrib['quality'] = 'experimental'

    # Add note on file being a generated one
    overlay.insert(0, ET.Comment('NOTE: This file is generated, do not edit.'))

# Transform 'overlays' tag
overlays.tag = 'repositories'

# Sort overlays
overlays[:] = sorted(overlays[:], key=lambda x: x.find('name').text.lower())


# Extend by format version
overlays.attrib['version'] = '1.0'


if opts.human:
    recurse_print(overlays)
else:
    indent(overlays)
    repositories_xml = open(repositories_xml_location, 'w')
    repositories_xml.write("""\
<?xml version="1.0" encoding="UTF-8"?>
<!-- $Header$ -->
<?xml-stylesheet href="/xsl/repositories.xsl" type="text/xsl"?>
<!DOCTYPE repositories SYSTEM "/dtd/repositories.dtd">
""")
    a.write(repositories_xml, encoding='utf-8')
    repositories_xml.close()
