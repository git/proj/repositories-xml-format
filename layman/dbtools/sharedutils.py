# Copyright (C) 2009 Sebastian Pipping <sebastian@pipping.org>
# Licensed under GPL 2 or later

import codecs

# From <http://effbot.org/zone/element-lib.htm>
# BEGIN
def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
# END

def recurse_print(e, level=0):
    t = e.text
    if t:
        if t == '' or t.isspace():
            t = None
        else:
            t = t.strip('\n\r')
    print '%s<%s>%s' % ('  '*level, e.tag, t and ('  .. %s' % to_ascii(t)) or '')
    for k, v in sorted(e.attrib.items()):
        print '%s| %s  = %s' % ('  '*(level+2), k, v)
    for c in e.getchildren():
        recurse_print(c, level=level + 1)

def to_ascii(o, current_encoding='utf-8'):
    if not isinstance(o, basestring):
        return o

    if isinstance(o, unicode):
        s = o
    else:
        s = unicode(o, current_encoding)
    return codecs.encode(s, 'ascii', 'ignore')
