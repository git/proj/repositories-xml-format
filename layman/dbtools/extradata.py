# Copyright (C) 2009 Sebastian Pipping <sebastian@pipping.org>
# Licensed under GPL 2 or later

QUALITY_CORE = 'core'
QUALITY_STABLE = 'stable'
QUALITY_TESTING = 'testing'
QUALITY_EXPERIMENTAL = 'experimental'
QUALITY_GRAVEYARD = 'graveyard'

TRANSITION_DATA_EXTRA = {
    'bazaar':{
        'owner':{
            'name':'Mark Lee',
            'email':'malept@malept.com',
        },
        'feeds':[
            'http://bazaar.launchpad.net/~serkan-kaba/serkan-overlay/mainline/atom',
        ],
    },
    'lila-theme':{
        'owner':{
            'email':'fosstux@gmail.com',
        },
    },
    'rbu':{
        'quality':QUALITY_EXPERIMENTAL,
        'owner':{
            'name':'Robert Buchholz',
        },
    },
    'sping':{
        'quality':QUALITY_EXPERIMENTAL,
        'owner':{
            'name':'Sebastian Pipping',
        },
    },
    'jyujin':{'feeds':['http://git.jyujin.de/?p=portage-overlay.git;a=atom']},
    'pioto-overlay':{'feeds':['http://git.pioto.org/gitweb/pioto-overlay.git?a=atom']},
    'thousand-parsec':{'feeds':['http://git.thousandparsec.net/gitweb/gitweb.cgi?p=gentoo-overlay.git;a=atom']},
    'hollow':{'feeds':['http://git.xnull.de/gitweb/?p=overlay.git;a=atom']},
    'zen-overlay':{'feeds':['http://git.zen-sources.org/?p=portage-overlay.git;a=atom']},
    'THE':{'feeds':['http://git.zen-sources.org/?p=THE.git;a=atom']},
    'lxde':{'feeds':['http://bitbucket.org/yngwin/lxde-overlay/atom/']},
    'njw':{'feeds':['http://git.njw.me.uk/cgit/cgit.cgi/njw-gentoo-local/atom/?h=master']},
    'oss-overlay':{'feeds':['http://hg.atheme.org/users/majeru/portage-overlay/atom-log']},
    'devnull':{'feeds':['http://hg.core.ws/devnull/atom-log']},
    'mamona':{'feeds':['http://rsalveti.net/git/?p=mamona-overlay;a=atom']},
    'voyageur':{'feeds':['http://cafarelli.fr/websvn/rss.php?repname=voyageur-overlay&path=&isdir=1']},
    'akoya':{'feeds':['https://hg.poildetroll.net/gentoo/overlay/atom-log']},
    'pd-overlay':{'feeds':['http://sourceforge.net/export/rss2_keepsake.php?group_id=180376']},
    'tryton':{'feeds':['http://hg.tryton.org/hgwebdir.cgi/tryton-overlay/atom-log']},
    'sabayon':{'feeds':['http://gitweb.sabayon.org/?p=overlay.git;a=atom']},
}

# List more or less from current <http://overlays.gentoo.org/>
TRANSITION_DATA_PROJECTS = set((
    'cell',
    'efika',
    'emacs',
    'finnish',
    'gnome',
    'gnustep',
    'hardened-development',
    'java-overlay',
    'kde-testing',
    'kolab',
    'mozilla',
    'mysql-testing',
    'nx',
    'perl-experimental',
    'php-4',
    'php-experimental',
    'php-testing',
    'postgresql-experimental',
    'postgresql-testing',
    'powerpc',
    'python',
    'rox',
    'ruby',
    'science',
    'sunrise',
    'toolchain',
    'vdr-devel',
    'vdr-experimental',
    'vdr-testing',
    'vdr-xine',
    'vmware',
    'voip',
    'vps',
    'webapps-experimental',
    'xen',
    'xfce-dev',
))
