# Copyright (C) 2009 Sebastian Pipping <sebastian@pipping.org>
# Licensed under GPL 2 or later

import re
FEED_EXTRACTORS = []

def _register_feed_extractor(pattern, format):
    regex = re.compile(pattern)
    FEED_EXTRACTORS.append({'regex':regex, 'format':format})


# git://github.com/dang/kvm.git
# http://github.com/feeds/dang/commits/kvm/master
_register_feed_extractor(
        '^[^ ]+://github.com/([^/]+)/([^/.]+)(?:.git)?$',
        'http://github.com/feeds/\\1/commits/\\2/master')


# git://gitorious.org/gentoo-multimedia/gentoo-multimedia.git
# http://gitorious.org/gentoo-multimedia.atom
_register_feed_extractor(
        '^[^ ]+://gitorious.org/([^/]+)/[^/.]+(?:.git)?$',
        'http://gitorious.org/\\1.atom')

# git://git.overlays.gentoo.org/dev/alexxy.git
# http://git.overlays.gentoo.org/gitweb/?p=dev/alexxy.git;a=atom
_register_feed_extractor(
        '^[^ ]+://git.overlays.gentoo.org/(dev|proj)/([^/.]+)(?:.git)?$',
        'http://git.overlays.gentoo.org/gitweb/?p=\\1/\\2.git;a=atom')

# git://repo.or.cz/dottout.git
# http://repo.or.cz/w/dottout.git?a=rss
_register_feed_extractor(
        '^[^ ]+://repo.or.cz/([^/.]+)(?:.git)?$',
        'http://repo.or.cz/w/\\1.git?a=rss')  # atom broken currently

# http://gentoo-china-overlay.googlecode.com/svn/trunk/
# http://code.google.com/feeds/p/gentoo-china-overlay/svnchanges/basic
_register_feed_extractor(
        '^[^ ]+://([^ ]+).googlecode.com/svn/.*$',
        'http://code.google.com/feeds/p/\\1/svnchanges/basic')

# https://arcon.googlecode.com/hg/
# http://code.google.com/feeds/p/arcon/hgchanges/basic
_register_feed_extractor(
        '^[^ ]+://([^ ]+).googlecode.com/hg/.*$',
        'http://code.google.com/feeds/p/\\1/hgchanges/basic')

# git://git.goodpoint.de/overlay-sping.git
# http://git.goodpoint.de/?p=overlay-sping.git;a=atom
_register_feed_extractor(
        '^[^ ]+://git.goodpoint.de/([^/.]+)(?:.git)?$',
        'http://git.goodpoint.de/?p=\\1.git;a=atom')


# svn://overlays.gentoo.org/proj/toolchain
# http://overlays.gentoo.org/proj/toolchain/timeline
_register_feed_extractor(
        '^svn://overlays.gentoo.org/(dev|proj)/([^/]+)(?:/.*)?$',
        'http://overlays.gentoo.org/\\1/\\2/timeline')
