#!/usr/bin/env python
# Copyright (C) 2010 Gentoo Foundation
# Written by Sebastian Pipping <sping@gentoo.org>
#
# Licensed under GPL v2 or later

from __future__ import print_function
import re


_repo_line = re.compile('^repo ([^ ]+)')
_terms_status_line = re.compile('^# gentoo-terms-status = (.+)$')
_overlay_marker_line = re.compile('^# gentoo-is-overlay = (True|False)')
_desc_line = re.compile('^(?:#gl2# )?([^# ][^ ]+) "(.+ <[^@]+@[^>]+>)" = "(.+)"')
_dont_add_line = re.compile('^# gentoo-dont-add-to-layman = (.+)')


def _parse_bool(text):
    if text == 'False':
        return False
    else:
        assert(text == 'True')
        return True


class RepoDatabase:
    def __init__(self):
        self._db = dict()

    def _add(self, repo, terms_status, is_overlay, dont_add_reason):
        if not repo or repo in self._db:
            return
        self._db[repo] = (terms_status, is_overlay, dont_add_reason, None, None)

    def _describe(self, repo, contact, desc):
        self._db[repo] = self._db[repo][0:3] + (contact, desc)

    def names(self):
        return self._db.keys()

    def data(self, repo):
        return self._db[repo]

    def feed(self, conf_filename):
        f = open(conf_filename, 'r')

        repo = None
        terms_status = None
        is_overlay = None
        dont_add_reason = None

        desc_map = dict()
        for l in f:
            line = l.rstrip('\n').lstrip()
            for matcher in (_repo_line, _terms_status_line, _overlay_marker_line, _desc_line, _dont_add_line):
                m = matcher.search(line)
                if m:
                    if matcher is _repo_line:
                        repo = m.group(1)

                    elif matcher is _terms_status_line:
                        terms_status = m.group(1)

                    elif matcher is _overlay_marker_line:
                        is_overlay = _parse_bool(m.group(1))

                    elif matcher is _desc_line:
                        desc_repo = m.group(1)
                        if repo != desc_repo:
                            print('WARNING: Looks like description of repo "%s" mentions repo "%s", by mistake' % (repo, desc_repo))
                        desc_contact = m.group(2)
                        desc_desc = m.group(3)
                        desc_map[repo] = (desc_contact, desc_desc)

                        self._add(repo, terms_status, is_overlay, dont_add_reason)
                        repo = None
                        terms_status = None
                        is_overlay = None
                        dont_add_reason = None

                    elif matcher is _dont_add_line:
                        dont_add_reason = m.group(1)

                    break
        f.close()

        self._add(repo, terms_status, is_overlay, dont_add_reason)

        for desc_repo, (desc_contact, desc_desc) in desc_map.items():
            self._describe(desc_repo, desc_contact, desc_desc)

    def _dump(self):
        for repo, (terms_status, is_overlay, dont_add_reason, contact, desc) in sorted(self._db.items()):
            print('repo %s' % repo)
            if terms_status:
                print('\t# gentoo-terms-status = %s' % terms_status)
            if is_overlay is not None:
                print('\t# gentoo-is-overlay = %s' % str(is_overlay))
            if dont_add_reason:
                print('\t# gentoo-dont-add-to-layman = %s' % dont_add_reason)
            if contact and desc:
                print('\t%s "%s" = "%s"' % (repo, contact, desc))
            print()
