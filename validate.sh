#!/usr/bin/env bash
for folder in . samples ; do
	printf '%-10s  ' ${folder}
	printf '%-20s  ' layman-global.txt
	printf '%-10s  ' DTD
	xmllint --noout --dtdvalid schema/layman-global.dtd ${folder}/layman-global.txt \
		&& echo VALID \
		|| echo BROKEN

	printf '%-10s  ' ${folder}
	printf '%-20s  ' layman-global.txt
	printf '%-10s  ' 'Relax NG'
	{
		xmllint --noout --relaxng schema/layman-global.rng ${folder}/layman-global.txt \
			&& echo VALID \
			|| echo BROKEN
	} |& grep -v ' validates$'

	printf '%-10s  ' ${folder}
	printf '%-20s  ' repositories.xml
	printf '%-10s  ' DTD
	xmllint --noout --dtdvalid schema/repositories.dtd ${folder}/repositories.xml \
		&& echo VALID \
		|| echo BROKEN

	printf '%-10s  ' ${folder}
	printf '%-20s  ' repositories.xml
	printf '%-10s  ' 'Relax NG'
	{
		xmllint --noout --relaxng schema/repositories.rng ${folder}/repositories.xml \
			&& echo VALID \
			|| echo BROKEN
	} |& grep -v ' validates$'
done
